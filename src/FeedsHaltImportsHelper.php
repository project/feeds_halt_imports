<?php

namespace Drupal\feeds_halt_imports;

class FeedsHaltImportsHelper {

  /**
   * Turn off relevant feeds
   */
  public function haltRelevantFeeds(){

    // Determine feeds to turn off
    $feedsSetToRunOnCron = $this->getFeedsSetToRunOnCron();

    // Record the feeds we're shutting off
    $this->saveRecordOfFeedsToBeDisabled($feedsSetToRunOnCron);

    // Shut the feeds off
    $this->removeQueueItems();
    $this->removeFeedProgressInfo();
    $this->disableFeeds($feedsSetToRunOnCron);

    // Provide a notice
    \Drupal::messenger()->addMessage(t('Successfully halted '.count($feedsSetToRunOnCron).' feeds.'));

  }


  /**
   * Turn back on those feeds
   */
  public function resumeRelevantFeeds(){

    // get record of what we disabled
    $disabledFeeds = $this->getRecordOfFeedsToBeDisabled();

    // Enable feeds we previously disabled
    $this->enableFeeds($disabledFeeds);

    // Notify user
    \Drupal::messenger()->addMessage(t('Successfully restarted '.count($disabledFeeds).' feeds.'));

  }


  /**
   * Get names of relevant feeds
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getFeedNamesSetToRunOnCron(){

    // Determine and load feeds
    $feedIds = $this->getFeedsSetToRunOnCron();
    $feeds = \Drupal::entityTypeManager()
      ->getStorage('feeds_feed')
      ->loadMultiple($feedIds);

    // Loop over feeds
    $titles = [];
    foreach($feeds as $feed) {
      $titles[] = $feed->get('title')->getValue()[0]['value'];
    }

    return $titles;

  }


  private function enableFeeds($disabledFeeds){

    // load entities
    $feeds = \Drupal::entityTypeManager()
      ->getStorage('feeds_feed')
      ->loadMultiple($disabledFeeds);

    // Loop over feeds and turn them on
    foreach($feeds as $feed) {
      $feed->set('status',1);
      $feed->save();
    }

  }


  /**
   *  Disable tha feeds
   */
  private function disableFeeds($feedsSetToRunOnCron){

    // Determine and load feeds
    $feeds = \Drupal::entityTypeManager()
      ->getStorage('feeds_feed')
      ->loadMultiple($feedsSetToRunOnCron);

    // Loop over feeds
    foreach($feeds as $feed) {
      $feed->set('status',0);
      $feed->save();
    }

  }


  /**
   * Remove relevant records from key_value
   */
  private function removeFeedProgressInfo(){
    $database = \Drupal::database();
    $numberOfItemsDeleted = $database->delete('key_value')
      ->condition('collection','feeds_feed.%', 'LIKE')
      ->execute();
  }


  /**
   * Drop Queue entries with a name like feeds_feed_refresh:%
   */
  private function removeQueueItems(){

    $database = \Drupal::database();
    $numberOfQueueItemsDeleted = $database->delete('queue')
      ->condition('name','feeds_feed_refresh:%', 'LIKE')
      ->execute();

  }


  /**
   * Save feeds to be disabled in key_value
   *
   * @param $feedsSetToRunOnCron
   */
  private function saveRecordOfFeedsToBeDisabled($feedsSetToRunOnCron) {
    if(count($feedsSetToRunOnCron)){
      \Drupal::state()->set('feeds_halt_imports.disabled_feeds',$feedsSetToRunOnCron);
    }
  }


  /**
   * Return record of the feeds that we turned off earlier
   *
   * @return mixed
   */
  private function getRecordOfFeedsToBeDisabled(){
    return \Drupal::state()->get('feeds_halt_imports.disabled_feeds', FALSE);
  }


  /**
   * Get relevant feed ids
   *
   * @return array|int
   */
  private function getFeedsSetToRunOnCron(){

    // first determine feed types set to run on a schedule
    $feedTypesSetToRunOnCron = $this->getFeedTypesSetToRunOnCron();

    // get and return feeds of those types that are enabled
    $query = \Drupal::entityQuery('feeds_feed');
    $query->condition('type', $feedTypesSetToRunOnCron, 'IN');
    $query->condition('status', 1);
    return $query->execute();

  }


  /**
   * Get the feed types set to keep running
   *
   * @return array
   */
  private function getFeedTypesSetToRunOnCron(){

    // Query config table to get feed types
    // @todo find a drupal way to get bundles
    $database = \Drupal::database();
    $query = $database->select('config');
    $query->condition('config.name', 'feeds.feed_type.%', 'LIKE');
    $query->fields('config', ['name','data']);
    $queryResults = $query->execute();

    // Loop over results collecting feed types set to run on cron
    $feedTypesSetToRunOnCron = [];
    foreach($queryResults as $result) {
      $resultData = unserialize($result->data);
      $importPeriod = $resultData['import_period'];
      if($importPeriod !== -1){
        $feedTypesSetToRunOnCron[] = str_replace('feeds.feed_type.','',$result->name);
      }
    }

    // Return the feed types
    return $feedTypesSetToRunOnCron;

  }


}