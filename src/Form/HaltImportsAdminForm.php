<?php

/**
 * @file
 * Contains Drupal\feeds_halt_imports\Form\HaltImportsAdminForm
 */
namespace Drupal\feeds_halt_imports\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class HaltImportsAdminForm extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'feeds_halt_imports.settings',
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'halt_imports_admin_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get config for this module
    $config = $this->config('feeds_halt_imports.settings');

    // If this form has not been filled out and saved then the import status is 1/TRUE/ON/CHECKED
    if($config->get('import_status') === null) {
      $config->set('import_status',1);
    }

    // Determine the feeds currently set to run on cron using a service from this module
    $haltImportshelperService = \Drupal::service('feeds_halt_imports.helper');
    $affectedFeedNames = $haltImportshelperService->getFeedNamesSetToRunOnCron();

    // Provide Info on Affected Feeds
    if(count($affectedFeedNames)) {
      // Provide warning with a list of the Feeds we'll be halting
      $form['warning'] = [
        '#type'   => 'container',
        '#markup' => '<b>Warning: The following feeds will be stopped if you proceed with this action:</b><ul><li>'
          . implode('</li><li>', $affectedFeedNames) . '</li></ul>',
      ];
    }else {
      // Provide warning with a list of the Feeds we'll be halting
      $form['warning'] = [
        '#type'   => 'container',
        '#markup' => '<b>No feeds set to run on cron are currently enabled.</b>'
      ];
    }


    // Vary the description of field based on status of field
    // (Indexes of 0 and 1 aka true and false)
    $importStatusFieldDescriptions = [
      'Check the box and save to halt all running feeds.',
      'Uncheck the box and save to halt all running feeds.',
    ];

    // Create the checkbox for toggling status
    $form['import_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import Status'),
      '#description' => $importStatusFieldDescriptions[$config->get('import_status')],
      '#default_value' => $config->get('import_status'),
    ];

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // If we're turning feeds off
    if(
      !$form_state->getValue('import_status')
      && $this->config('feeds_halt_imports.settings')->get('import_status')
    ) {
      $haltImportshelperService = \Drupal::service('feeds_halt_imports.helper');
      $haltImportshelperService->haltRelevantFeeds();
    }

    // If we're turning feeds on
    if(
      $form_state->getValue('import_status')
      && !$this->config('feeds_halt_imports.settings')->get('import_status')
    ) {
      $haltImportshelperService = \Drupal::service('feeds_halt_imports.helper');
      $haltImportshelperService->resumeRelevantFeeds();
    }

    // Save the import status
    $this->config('feeds_halt_imports.settings')
      ->set('import_status', $form_state->getValue('import_status'))
      ->save();
  }


}